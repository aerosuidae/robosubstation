data:extend({
  {
    type = "bool-setting",
    name = "robosubstation-compat-LightedPolesPlus",
    order = "a",
    setting_type = "runtime-global",
    default_value = true,
  },
})
