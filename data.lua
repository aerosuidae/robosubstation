data:extend({
	{
    icon = "__robosubstation__/icon.png",
    icon_size = 32,
    name = "robosubstation",
		order = data.raw.item.substation.order .. "b",
    place_result = "robosubstation",
    stack_size = 50,
    subgroup = "energy-pipe-distribution",
    type = "item",
  },
	{
		type = "recipe",
		name = "robosubstation",
		category = "crafting",
    subgroup = "energy-pipe-distribution",
		enabled = false,
    icon = "__robosubstation__/icon.png",
		icon_size = 32,
		ingredients = {
			{ type = "item", name = "substation", amount = 1 },
			{ type = "item", name = "accumulator", amount = 1 },
			{ type = "item", name = "advanced-circuit", amount = 5 },
		},
		results = {
			{ type = "item", name = "robosubstation", amount = 1 },
		},
		hidden = false,
		energy_required = 1.0,
		order = data.raw.item.substation.order .. "b",
	},
})

local ss = table.deepcopy(data.raw["electric-pole"]["substation"])
ss.name = "robosubstation"
ss.icon = "__robosubstation__/icon.png"
ss.icon_size = 32
ss.minable.result = "robosubstation"
ss.fast_replaceable_group = "substations"
data:extend({ss})

data:extend({
	{
		type = "roboport",
		name = "robosubstation-charger",
    icon = "__robosubstation__/icon.png",
		icon_size = 32,
		flags = {
			"not-blueprintable",
			"not-deconstructable",
		},
		energy_source = {
			type = "electric",
			usage_priority = "secondary-input",
			input_flow_limit = "4MW",
			buffer_capacity = "10MJ",
		},
		selection_box = {
			{ x = 0, y = 0 },
			{ x = 0.75, y = 0.75 },
		},
		energy_usage = "10kW",
		charging_energy = "1MW",
		recharge_minimum = "4MJ",
		logistics_radius = 9,
		construction_radius = 20,
		charge_approach_distance = 3.5,
		robot_slots_count = 0,
		material_slots_count = 0,
		request_to_open_door_timeout = 15,
		spawn_and_station_height = -0.1,
    charging_offsets = {
			{ -0.75, -2 },
      { 0.75, -2 },
      { 0.75, -0.5 },
      {-0.75, -0.5 },
    },
		base = {
			filename = "__robosubstation__/nothing.png",
			width = 32,
			height = 32,
		},
		base_patch = {
		  filename = "__robosubstation__/nothing.png",
		  priority = "medium",
			width = 32,
			height = 32,
		  frame_count = 1,
		},
    base_animation = {
      animation_speed = 0.5,
      filename = "__robosubstation__/base-animation.png",
      frame_count = 8,
      height = 31,
      hr_version = {
        animation_speed = 0.5,
        filename = "__robosubstation__/hr-base-animation.png",
        frame_count = 8,
        height = 59,
        priority = "medium",
        scale = 0.5,
        shift = { 0, -2.5 },
        width = 83
      },
      priority = "medium",
      shift = { 0, -2.5 },
      width = 42
    },
		door_animation_up = {
			filename = "__robosubstation__/nothing.png",
			priority = "medium",
			width = 32,
			height = 32,
			frame_count = 1,
			animation_speed = 1,
		},
		door_animation_down = {
			filename = "__robosubstation__/nothing.png",
			priority = "medium",
			width = 32,
			height = 32,
			frame_count = 1,
			animation_speed = 1,
		},
    recharging_animation = {
      animation_speed = 0.5,
      filename = "__base__/graphics/entity/roboport/roboport-recharging.png",
      frame_count = 16,
      height = 35,
      priority = "high",
      width = 37,
    },
		working_sound = {
			sound = { filename = "__base__/sound/roboport-working.ogg", volume = 0 },
			max_sounds_per_type = 3,
			audible_distance_modifier = 0.5,
			probability = 1 / (5 * 60),
		},
		recharging_light = { intensity = 0.6, size = 5 },
		vehicle_impact_sound = { filename = "__base__/sound/car-metal-impact.ogg", volume = 0.65 },
		radius_visualisation_picture = {
			filename = "__base__/graphics/entity/roboport/roboport-radius-visualization.png",
			width = 12,
			height = 12,
			priority = "extra-high-no-scale"
		},
		construction_radius_visualisation_picture = {
			filename = "__base__/graphics/entity/roboport/roboport-construction-radius-visualization.png",
			width = 12,
			height = 12,
			priority = "extra-high-no-scale"
		},
	},
	{
		type = "radar",
		name = "robosubstation-radar",
    icon = "__robosubstation__/icon.png",
		icon_size = 32,
		max_distance_of_sector_revealed = 0,
		max_distance_of_nearby_sector_revealed = 1,
		energy_usage = "10kW",
    energy_per_nearby_scan = "1kJ",
    energy_per_sector = "1kJ",
    energy_source = {
      type = "electric",
      usage_priority = "secondary-input",
    },
		flags = {
			"not-blueprintable",
			"not-deconstructable",
		},
		pictures = {
			layers = {
				{
					filename = "__robosubstation__/nothing.png",
					width = 32,
					height = 32,
					direction_count = 1,
				},
			},
		},
	},
})
