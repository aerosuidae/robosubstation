
local function is_robosubstation(entity)
	return entity.name == "robosubstation"
		or (settings.global["robosubstation-compat-LightedPolesPlus"].value and entity.name == "lighted-robosubstation")
end

local function OnEntityCreated(event)
	local entity = event.created_entity or event.entity

	if not entity or not entity.valid then
		return
	end

	if is_robosubstation(entity) then
		local charger = entity.surface.create_entity({
			name = "robosubstation-charger",
			position = entity.position,
			force = entity.force,
		})
		charger.operable = false
		charger.destructible = false
		local radar = entity.surface.create_entity({
			name = "robosubstation-radar",
			position = entity.position,
			force = entity.force,
		})
		radar.operable = false
		radar.destructible = false
	end
end

local function OnEntityRemoved(event)
	local entity = event.entity

	if not entity or not entity.valid then
		return
	end

	if is_robosubstation(entity) then
		local fakes = entity.surface.find_entities_filtered({
			name = { "robosubstation-charger", "robosubstation-radar" },
			area = {
				{ x = entity.position.x - 0.1, y = entity.position.y - 0.1 },
				{ x = entity.position.x + 0.1, y = entity.position.y + 0.1 },
			},
		})
		for _, charger in ipairs(fakes) do
			charger.destroy()
		end
	end
end

local function handlers()
	script.on_event({defines.events.on_built_entity, defines.events.on_robot_built_entity, defines.events.script_raised_built}, OnEntityCreated)
	script.on_event({defines.events.on_player_mined_entity, defines.events.on_robot_pre_mined, defines.events.on_entity_died, defines.events.script_raised_destroy}, OnEntityRemoved)
end

script.on_init(handlers)
script.on_load(handlers)
