
local substation = data.raw["electric-pole"]["substation"]
local robosubstation = data.raw["electric-pole"]["robosubstation"]

substation.fast_replaceable_group = substation.fast_replaceable_group or "substations"
robosubstation.fast_replaceable_group = substation.fast_replaceable_group

if not substation.next_upgrade then
	substation.next_upgrade = "robosubstation"
end
